# *RDFCMAP*
*RDFCMAP* converts between CMap CXL and RDF TTL. 

<img alt="rdfcmap" src="img/rdfcmap.png" width="1000px"/>

## There are two options to get *RDFCMAP*: downloading a precompiled jar or building your own.

### Obtain precompiled version
Prerequisites:<br/>
1. You need access to file repository of Allotrope ([join Allotrope](https://www.allotrope.org/join-us)).
2. You need to have [Java8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed.
3. You need to have [Maven](https://maven.apache.org/download.cgi) installed.

Execute following steps:
1. Download [archive from file repository](https://highq.in/7hbrpahuxa).
2. Extract downloaded archive
3. Open a Maven console in the folder with the extracted artifacts and execute `mvn dependency:copy-dependencies`
4. Maven will start downloading all required dependencies to folder `target/dependency`.
5. Move all downloaded dependencies from folder `target/dependency` to folder `lib` next to rdfcmap-X.Y.Z.jar: 
```
mkdir lib
copy target\lib\*.* lib
```

### Build your own version of *RDFCMAP*
Prerequisites:<br/>
1. You need to have [Java8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed.
2. You need to have [Maven](https://maven.apache.org/download.cgi) installed.

In order to build *RDFCMAP* you can execute following steps:
*  Check out this repository
*  Run `mvn package`
*  Find the compiled jar in folder `target` and all its dependencies in folder `target/lib`

## Usage to convert from CXL to TTL
Execute from the command line (replace X.Y.Z with the correct version number of your jar):
```
c:\path\to\jdk8\bin\java -jar rdf-cmap-X.Y.Z.jar -i your-input-cmap-model.cxl
```

Have a look at an *[example](example.md)*.

### Apply readable labels
*RDFCMAP* can create readable IRIs instead of UUIDs and blank nodes. Execute from the command line:
```
c:\path\to\jdk8\bin\java -jar rdf-cmap-X.Y.Z.jar -i your-input-cmap-model.cxl -ru -rb
```

*Note: Conversion from CXL to TTL supports vocabulary of AFO2.0 as well as versions greater than CR/2018/07*

Use ```--help``` to see available options
```
c:\path\to\jdk8\bin\java -jar rdf-cmap-X.Y.Z.jar --help
```

## How to label nodes? How to serialize RDF with named individuals with user-defined labels?

You can define IRIs for instances by adding a label with leading `$` to nodes of a cmap, e.g. `$sequencetable`. *RDFCMAP* will create an IRI of `ex:Sequencetable` in this case.

For example, the node shown below is converted from CXL to the triple: 
```
ex:Sequencetable rdf:type af-r:AFR_0000945
```

<img alt="rdfcmap" src="img/labeled-node.png" width="215px"/>

## How to batch transform a folder?

1.  Put all CXL files into the same folder for conversion.
2.  Provide the absolute path to the folder as input to *RDFCMAP*.
3.  After successful conversion, all triples will be contained in file `merged-files.ttl`.

Example: 

```
c:\path\to\jdk8\bin\java -jar rdf-cmap-X.Y.Z.jar -i "C:\path\to\input\folder"
```

## How to specify a custom namespace?

Provide your custom namespace as first argument of the command line with option `--prefix`. 

Example: 
```
c:\path\to\jdk8\bin\java -jar rdf-cmap-X.Y.Z.jar --prefix "my=http://www.mydomain.org/namespace#" -i your-input-cmap-model.cxl -ru -rb
```

## How to create a cmap to be used with *RDFCMAP*?

1.  Get CMAP Tools from https://cmap.ihmc.us/
2.  Create a new cmap following guidelines below on formatting, see [documentation of ADM](https://gitlab.com/allotrope/adm-patterns/tree/master/Legend) for details about formatting of Allotrope data models 
3.  All nodes of a cmap model are transformed to instances in RDF model with rdf:type of the class that is identified by the label of the cmap node.
4.  Links between nodes in a cmap are transformed to object properties in RDF model.
5.  Labels of nodes and links must agree to existing labels (skos:prefLabel) of classes and properties in RDF vocabulary. 
Alternatively, IRIs of classes and properties can be specified in round braces. By default, 
*RDFCMAP* transforms cmap nodes into named resources with arbitrary UUIDs. If specific UUIDs or defined IRIs are to be used for created instances
then they have to be added to labels of cmap nodes with `$` as prefix. For example, `$MyNode` or `$urn:uuid:6488a688-88a4-40b5-9e52-a817f451b0d1`.   
6.  Cardinalities can be added to object properties in cmap: default without specification of cardinality is "min 0". Exact cardinality is specified by adding the number, e.g. "has part =1" means there is exactly 1 link of "has part". Minimum cardinality is specified by ">1" (one or more)
7.  Export cmap as CXL file

## How to create a SPARQL query?

1. Prepare a CMAP
  1. Change style of your root node to **oval** with **dashed** border. 
  2. Change style of your target node to **oval** with **solid** border.
     -  Each SPARQL query is created as a graph starting from a specified root node to a specified target node.
     -  By default all values of properties of the target node will be returned as query result
  3. Delete all nodes from your cmap that do not need to be included in your query graph 
     -  *Keep all nodes on the path from root node to target node*
	 -  *Keep further nodes if required for disambiguation*
  4. Save CMAP and export as CXL
2. Execute *RDFCMAP* with following command line parameters
  - ```c:\path\to\jdk8\bin\java -jar rdf-cmap-X.Y.Z.jar -i your-cropped-query-graph-from-cmap.cxl -r vocabulary.ttl --sparql```
  - ```vocabulary.ttl``` *is an optionally specified file containing vocabulary that helps to create better understandable queries*
3. Resulting SPARQL query is created next to your input file.

## Where do I get support?
*RDFCMAP* is developed by Helge Krieg, [OSTHUS GmbH](http://www.osthus.com). Please contact helge&nbsp;.&nbsp;krieg&nbsp;@&nbsp;osthus&nbsp;.&nbsp;com if you have questions to application or extension of *RDFCMAP*.
If you find issues or have feature requests please post to the issue tracker. We are happy to support!

## What is the license?
*RDFCMAP* is published under the [LGPL3 license](LICENSE).


# Changelog:

[V2.6.7](https://highq.in/7hbrpahuxa)
* support modularized cmaps
* batch transform an input folder
* support custom prefixes
* clean prefix header

[V2.6.5](https://highq.in/5sh6kpx5yl)
* added af-sp prefix
* use localname of IRIs for readable labels
* allow user-specified UUIDs
* allow user-specified labels
* improved readable IDs
* improved handling of typed literals
* support property paths
* removed dependency to ADF API (removed ADF export)

V2.4.2
* updated creation of SPARQL to support LC-UV model CR/2018/07

V2.4.1
* fixed IRIs of CHEBI instances 

V2.4.0
* improved processing of labels 
* support LC-UV model CR/2018/07

V2.3.4
* improved sparql export
* improved handling of user-specified prefix mapping
 
V2.3.2
* Create visualization directly from ADF
* Note: Reading and writing ADF requires presence of ADF-API in build path. Please contact OSTHUS for further support and information (office(at)osthus(dot)com).  

V2.3.1 
* Improved conversion from ttl to cmap in order to support blank nodes and literal values
* Improved import of triples
* Note: Conversion from TTL to CXL requires layouting of concepts in order to get a useful visualization. Enabling automatic layouting with *RDFCMAP* needs additional third-party dependencies. Currently, *RDFCMAP* supports layouting based on graphviz as well as different layout algorithms provided by gephi. Please contact OSTHUS for further information (office(at)osthus(dot)com).  

V2.2.0 
* Updated conversion from ttl to cmap in order to support BFO aligned models
* Note: Conversion from TTL to CXL requires layouting of concepts in order to get a useful visualization. Enabling automatic layouting with *RDFCMAP* needs additional third-party dependencies. Currently, *RDFCMAP* supports layouting based on graphviz as well as different layout algorithms provided by gephi. Please contact OSTHUS for further information (office(at)osthus(dot)com).  
  
V2.1.4 
* Updated conversion from cmap to ttl in order to support BFO aligned models
* Added SPARQL export
* Added Shape export
* Added ADF export

&copy;OSTHUS 2016-2019


 
# 1. Prepare your conceptual map (CMAP)
Create a CMAP with [CMAP tools](https://cmap.ihmc.us/).<br/>
Make sure to apply the correct formatting as described in the [legend of Allotrope Data Models](https://gitlab.com/allotrope/adm-patterns/tree/master/Legend). Instances are nodes with sharp edges, classes have rounded corners. Add persistent IRIs to each node and link surrounded by round braces. For terms from [AFO](http://purl.allotrope.org) and OBO ontologies, local names are sufficient (artificial identifiers such as `AFE_0000748`). 

**[example.cmap](example/example.cmap)**

<img alt="example" src="img/example.svg"/>

# 2. Export the conceptual map

In CMAP tools, select `File`from the menu, `Export as...`, `CXL...`. Save the exported CXL file in a folder, e.g. `C:\example\example.cxl`.

**[example.cxl](example/example.cxl)** 

# 3. Transform example.cxl to example.ttl

1. Open a command line (Windows+S, enter "cmd", hit return)
2. Navigate to your folder containing rdfcmap-X.Y.Z.jar e.g. `C:\rdfcmap`. Make sure that all required dependencies are located in folder `lib` next to rdf-cmap-X.Y.Z.jar. 
```cd C:\rdfcmap```
3. Execute *RDFCMAP*, make sure that `java` is in your `PATH`<br/>
```
java -jar rdf-cmap-X.Y.Z.jar -i "c:\example\example.cxl" -ru -rb -r "c:\example\voc.ttl"
```
Provide required vocabulary with option `-r` as absolute path in double quotes. Optionally apply parameters `-ru` and `-rb` for replacing UUIDs and blank node identifiers with better understandable IRIs.

4. After successful transformation you will find the TTL code of instance data in file `separate files\example-instance-model-human-readable.ttl`.

**[example-instance-model-human-readable.ttl](example/example-instance-model-human-readable.ttl)**

package com.osthus.rdfcmap.helper;

import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import com.osthus.rdfcmap.cmap.UserLabel;

/**
 * @author Helge Krieg, OSTHUS GmbH
 */
public class VisualizationInfoBuilderResult
{
	private Model model;
	private List<Resource> resources;
	private Map<String, UserLabel> uuid2userLabel;

	public VisualizationInfoBuilderResult(Model model, List<Resource> resources, Map<String, UserLabel> uuid2userLabel)
	{
		super();
		this.model = model;
		this.resources = resources;
		this.uuid2userLabel = uuid2userLabel;
	}

	public Model getModel()
	{
		return model;
	}

	public void setModel(Model model)
	{
		this.model = model;
	}

	public List<Resource> getResources()
	{
		return resources;
	}

	public void setResources(List<Resource> resources)
	{
		this.resources = resources;
	}

	public Map<String, UserLabel> getUuid2userLabel()
	{
		return uuid2userLabel;
	}

	public void setUuid2label(Map<String, UserLabel> uuid2userLabel)
	{
		this.uuid2userLabel = uuid2userLabel;
	}
}

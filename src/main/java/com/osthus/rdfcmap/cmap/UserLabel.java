package com.osthus.rdfcmap.cmap;

public class UserLabel
{
	private String label;
	private boolean isBlank;

	public UserLabel(String label, boolean isBlank)
	{
		super();
		this.label = label;
		this.isBlank = isBlank;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public boolean isBlank()
	{
		return isBlank;
	}

	public void setBlank(boolean isBlank)
	{
		this.isBlank = isBlank;
	}

}
